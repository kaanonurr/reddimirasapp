﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReddiMiras.Models.Model;

namespace ReddiMiras.Models.DataContext
{
    public class ReddiMirasDBContext:DbContext
    {
        public ReddiMirasDBContext()
        {
        }

        public ReddiMirasDBContext(DbContextOptions<ReddiMirasDBContext> options)
            : base(options)
        {

        }

        public DbSet<Users> Users { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReddiMiras.Models.Model
{
    public class Users
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Email { get; set; }
        [Required(ErrorMessage = "Şifreniz en az 7 karakter içermelidir.")]
        [StringLength(70, MinimumLength = 7)]
        public string Password { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string ActivationCode { get; set; }
        [Required(ErrorMessage = "TC Kimlik Numarası 11 karakterli olmalıdır.")]
        [StringLength(11, MinimumLength = 11)]
        public string TCNo { get; set; }
        public string CompanyName { get; set; }
        public string BillingAddress { get; set; }
        public string TaxNumber { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public DateTime BirthDate { get; set; }
        public string DegreeOfProximity { get; set; }
        public string BasisNo { get; set; }
        public string Decision { get; set; }
        public string NameSurname { get; set; }
        public string TaxOffice { get; set; }
        public string DeathName { get; set; }
        public string DeathSurname { get; set; }
        public string DeathTCNo { get; set; }
        public DateTime DeathBirthDate { get; set; }
        public string File { get; set; }
    }
}

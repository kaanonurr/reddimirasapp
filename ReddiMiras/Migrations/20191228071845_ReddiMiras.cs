﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ReddiMiras.Migrations
{
    public partial class ReddiMiras : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: false),
                    Password = table.Column<string>(maxLength: 70, nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    ActivationCode = table.Column<string>(nullable: true),
                    TcKNo = table.Column<string>(maxLength: 11, nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    InvoiceAdress = table.Column<string>(nullable: true),
                    TaxIdentityNo = table.Column<string>(nullable: true),
                    TelNo = table.Column<string>(nullable: true),
                    MobileNo = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    DegreeOfProximity = table.Column<string>(nullable: true),
                    BasisNo = table.Column<string>(nullable: true),
                    Decision = table.Column<string>(nullable: true),
                    NameSurname = table.Column<string>(nullable: true),
                    TaxAdministration = table.Column<string>(nullable: true),
                    DeathName = table.Column<string>(nullable: true),
                    DeathSurname = table.Column<string>(nullable: true),
                    DeathTcKNo = table.Column<string>(nullable: true),
                    DeathBirthDate = table.Column<DateTime>(nullable: false),
                    File = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}

#pragma checksum "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\Query\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "584aa639813148a8ee264051f62c1c86dd45e024"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Query_Index), @"mvc.1.0.view", @"/Views/Query/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Query/Index.cshtml", typeof(AspNetCore.Views_Query_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\_ViewImports.cshtml"
using ReddiMiras;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"584aa639813148a8ee264051f62c1c86dd45e024", @"/Views/Query/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8940a95251ffaf0aa97b8c19950209d2693a0684", @"/Views/_ViewImports.cshtml")]
    public class Views_Query_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-inline row"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\Query\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_QueryPage.cshtml";

#line default
#line hidden
            BeginContext(93, 601, true);
            WriteLiteral(@"


<style>
    body {
        background-image: none !important;
    }

    footer {
        bottom: auto;
    }

    .detail-content {
        height: auto;
        min-height: 700px;
    }
</style>
<div class=""breadcrumb"">
    <div class=""container"">
        <div class=""text-right"">
            <h1>Türkoğlu Avukatlık Bürosu</h1>
            <h2>TC SORGULAMA EKRANI</h2>

            <h3>Hesabınızda</h3>
            <p>55 Kontür / 55 Sorgulama Bulunmaktadır.</p>
        </div>
    </div>
</div>

<div class=""container detail-content"">
    <div class=""row"">
        ");
            EndContext();
            BeginContext(695, 38, false);
#line 37 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\Query\Index.cshtml"
   Write(await Component.InvokeAsync("LeftNav"));

#line default
#line hidden
            EndContext();
            BeginContext(733, 101, true);
            WriteLiteral("\r\n\r\n        <div class=\"col-lg-8 col-md-12\">\r\n            <h1>TC. Sorgulama Ekranı</h1>\r\n            ");
            EndContext();
            BeginContext(834, 374, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "584aa639813148a8ee264051f62c1c86dd45e0244883", async() => {
                BeginContext(864, 337, true);
                WriteLiteral(@"
                <div class=""form-group mb-2  col-10"">
                    <input type=""text"" class=""form-control col-12"" id="""" placeholder=""TC. Kimlik Numarası Gir"">
                </div>
                <button type=""button"" class=""btn btn-primary mb-2"" data-toggle=""modal"" data-target=""#konturzero"">Sorgula</button>
            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1208, 5206, true);
            WriteLiteral(@"
            <p>Yukarıdaki sorgulanacak tc. esas no su ve karar nosu olan tclerdir, dosya ekleyen kişi kendi adına bir reddi miras dosyası yüklemediyse eğer,dosya ekleyen kişinin tc si sorgulansa dahi sorgulama sonucu, sonuç bulunamadı olarak gözükecektir. </p>
            <div class=""alert alert-primary"" role=""alert"">
                Aranan kriterlere göre bir sonuç bulunamamıştır.
            </div>
            <hr>

            <div class=""alert alert-success"" role=""alert"">
                Bu dosyayı ekleyen kişiye ait başka dosyalarda bulunmuştur, tüm dosyaları görmek için <a href="""">tıkla.</a>
            </div>
            <p>* Tıklama sonrası eklenen kaç adet TC Dökümanı var ise o kadar Kontür hesabınızdan düşürülecektir.</p>
            <hr>
            <div class=""responsive-table"">
                <table class=""table table-striped table-dark"">
                    <thead>
                        <tr>
                            <th scope=""col"">TC.</th>
                            <th");
            WriteLiteral(@" scope=""col"">Doğum T.</th>
                            <th scope=""col"">Tel</th>
                            <th scope=""col"">Esas No</th>
                            <th scope=""col"">Karar No</th>
                            <th scope=""col"">Ad / Soyad</th>
                            <th scope=""col"">Dosya</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope=""row"">34798612886</th>
                            <td>21/11/1987</td>
                            <td>05322001146</td>
                            <td>39204</td>
                            <td>234242</td>
                            <td>Süha Uzun</td>
                            <td><a href=""#"">İndir</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class=""accordion"" id=""accordionExample"">
                <div class=""card"">
                    <div cla");
            WriteLiteral(@"ss=""card-header"" id=""headingZero"">
                        <h2 class=""mb-0"">
                            <button class=""btn btn-link"" type=""button"" data-toggle=""collapse"" data-target=""#collapseZero"" aria-expanded=""true"" aria-controls=""collapseZero"">
                                Varis Bilgileri
                            </button>
                        </h2>
                    </div>

                    <div id=""collapseZero"" class=""collapse show"" aria-labelledby=""headingZero"" data-parent=""#accordionExample"">
                        <div class=""card-body"">
                            <p>Adı : </p>
                            <p>Soyadı : </p>
                            <p>TC : </p>
                            <p>Doğum Tarihi : </p>
                            <p>Mail Adresi : </p>
                            <p>Cep No : </p>
                            <p>Yakınlık Derecesi : </p>
                        </div>
                    </div>
                </div>
                <div cl");
            WriteLiteral(@"ass=""card"">
                    <div class=""card-header"" id=""headingOne"">
                        <h2 class=""mb-0"">
                            <button class=""btn btn-link"" type=""button"" data-toggle=""collapse"" data-target=""#collapseOne"" aria-expanded=""false"" aria-controls=""collapseOne"">
                                Vefat Eden Kişinin Bilgileri
                            </button>
                        </h2>
                    </div>

                    <div id=""collapseOne"" class=""collapse"" aria-labelledby=""headingOne"" data-parent=""#accordionExample"">
                        <div class=""card-body"">
                            <p>Adı : </p>
                            <p>Soyadı : </p>
                            <p>TC : </p>
                            <p>Doğum Tarihi : </p>
                        </div>
                    </div>
                </div>
                <div class=""card"">
                    <div class=""card-header"" id=""headingTwo"">
                        <h2 class");
            WriteLiteral(@"=""mb-0"">
                            <button class=""btn btn-link collapsed"" type=""button"" data-toggle=""collapse"" data-target=""#collapseTwo"" aria-expanded=""false"" aria-controls=""collapseTwo"">
                                Dosyayı Ekleyen Kişinin Bilgileri
                            </button>
                        </h2>
                    </div>
                    <div id=""collapseTwo"" class=""collapse"" aria-labelledby=""headingTwo"" data-parent=""#accordionExample"">
                        <div class=""card-body"">
                            <p>Adı : </p>
                            <p>Soyadı : </p>
                            <p>TC : </p>
                            <p>Doğum Tarihi : </p>
                            <p>Mail Adresi : </p>
                            <p>Cep No : </p>
                            <p>Eklenen dosya sayısı : 7</p>
                            <p>Yakınlık Derecesi : </p>
                        </div>
                    </div>
                </div>

           ");
            WriteLiteral(" </div>\r\n\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"clearfix\">...</div>\r\n\r\n");
            EndContext();
            BeginContext(6415, 37, false);
#line 155 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\Query\Index.cshtml"
Write(await Component.InvokeAsync("Footer"));

#line default
#line hidden
            EndContext();
            BeginContext(6452, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(6455, 38, false);
#line 156 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\Query\Index.cshtml"
Write(await Component.InvokeAsync("Inquiry"));

#line default
#line hidden
            EndContext();
            BeginContext(6493, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(6496, 38, false);
#line 157 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\Query\Index.cshtml"
Write(await Component.InvokeAsync("Citizen"));

#line default
#line hidden
            EndContext();
            BeginContext(6534, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591

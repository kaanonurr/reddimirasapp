#pragma checksum "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\AllQuery\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7abd0b456a6ed8af369b3d55d1427753a6fb6afd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_AllQuery_Index), @"mvc.1.0.view", @"/Views/AllQuery/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/AllQuery/Index.cshtml", typeof(AspNetCore.Views_AllQuery_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\_ViewImports.cshtml"
using ReddiMiras;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7abd0b456a6ed8af369b3d55d1427753a6fb6afd", @"/Views/AllQuery/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8940a95251ffaf0aa97b8c19950209d2693a0684", @"/Views/_ViewImports.cshtml")]
    public class Views_AllQuery_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\AllQuery\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_QueryPage.cshtml";

#line default
#line hidden
            BeginContext(93, 599, true);
            WriteLiteral(@"
<style>

    body {
        background-image: none !important;
    }

    footer {
        bottom: auto;
    }

    .detail-content {
        height: auto;
        min-height: 700px;
    }
</style>
<div class=""breadcrumb"">
    <div class=""container"">
        <div class=""text-right"">
            <h1>Türkoğlu Avukatlık Bürosu</h1>
            <h2>TC SORGULAMA EKRANI</h2>

            <h3>Hesabınızda</h3>
            <p>55 Kontür / 55 Sorgulama Bulunmaktadır.</p>
        </div>
    </div>
</div>

<div class=""container detail-content"">
    <div class=""row"">
        ");
            EndContext();
            BeginContext(693, 38, false);
#line 36 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\AllQuery\Index.cshtml"
   Write(await Component.InvokeAsync("LeftNav"));

#line default
#line hidden
            EndContext();
            BeginContext(731, 4516, true);
            WriteLiteral(@"
        <div class=""col-lg-8 col-md-12"">
            <h1>Tümünü Sorgula</h1>
            <div class=""row"">
                <div class=""col-6"">
                    <p class=""card-text"">Elindeki TC. Listesini Yükle</p>
                    <input type=""file"" class=""form-control-file"" id=""exampleFormControlFile1"">
                </div>
                <div class=""col-6"">
                    <p class=""card-text"">Örnek excel dökümanını indirebilirsiniz.</p>
                    <a href=""#"" class=""btn btn-success"">İndir</a>
                </div>
            </div>

            <hr>
            <p> Excel listeniz içerisinden 7 adet sorgulanacak TC bulunmuştur. </p>
            <div class=""alert alert-success"" role=""alert"">
                4 Adet TC. nin karşılığı aşağıda listelenmiştir.
            </div>


            <div class=""responsive-table"">
                <table class=""table table-striped table-dark"">
                    <thead>
                        <tr>
                       ");
            WriteLiteral(@"     <th scope=""col"">TC.</th>
                            <th scope=""col"">Doğum T.</th>
                            <th scope=""col"">Tel</th>
                            <th scope=""col"">Esas No</th>
                            <th scope=""col"">Karar No</th>
                            <th scope=""col"">Ad / Soyad</th>
                            <th scope=""col"">Detay</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope=""row"">34798612886</th>
                            <td>05322001146</td>
                            <td>39204</td>
                            <td>234242</td>
                            <td>Süha Uzun</td>
                            <td>İndir</td>
                            <td><a href=""/gecmissorgulardetay"">Detay</a></td>
                        </tr>
                        <tr>
                            <th scope=""row"">34798612886</th>
                            <td");
            WriteLiteral(@">05322001146</td>
                            <td>39204</td>
                            <td>234242</td>
                            <td>Süha Uzun</td>
                            <td>İndir</td>
                            <td><a href=""/gecmissorgulardetay"">Detay</a></td>
                        </tr>
                        <tr>
                            <th scope=""row"">34798612886</th>
                            <td>05322001146</td>
                            <td>39204</td>
                            <td>234242</td>
                            <td>Süha Uzun</td>
                            <td>İndir</td>
                            <td><a href=""/gecmissorgulardetay"">Detay</a></td>
                        </tr>
                        <tr>
                            <th scope=""row"">34798612886</th>
                            <td>05322001146</td>
                            <td>39204</td>
                            <td>234242</td>
                            <td>Süha Uzun</td>
  ");
            WriteLiteral(@"                          <td>İndir</td>
                            <td><a href=""/gecmissorgulardetay"">Detay</a></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class=""alert alert-danger"" role=""alert"">
                3 adet TC. bulunamamıştır. Bulunamayan TC. lerin listesini görmek için tıkla.
            </div>

            <div class=""responsive-table"">
                <table class=""table table-striped table-dark"">
                    <thead>
                        <tr>
                            <th scope=""col"">TC.</th>
                            <th scope=""col"">Durum</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope=""row"">34798612886</th>
                            <td>Bulunamadı</td>
                        </tr>
                        <tr>
                            <th scope=""row"">34798");
            WriteLiteral(@"612886</th>
                            <td>Bulunamadı</td>
                        </tr>
                        <tr>
                            <th scope=""row"">34798612886</th>
                            <td>Bulunamadı</td>
                        </tr>
                    </tbody>
                </table>

            </div>


        </div>
    </div>
</div>

<div class=""clearfix"">...</div>

");
            EndContext();
            BeginContext(5248, 37, false);
#line 148 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\AllQuery\Index.cshtml"
Write(await Component.InvokeAsync("Footer"));

#line default
#line hidden
            EndContext();
            BeginContext(5285, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(5288, 38, false);
#line 149 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\AllQuery\Index.cshtml"
Write(await Component.InvokeAsync("Inquiry"));

#line default
#line hidden
            EndContext();
            BeginContext(5326, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(5329, 38, false);
#line 150 "C:\Users\kaano\Desktop\reddimirasapp\ReddiMiras\Views\AllQuery\Index.cshtml"
Write(await Component.InvokeAsync("Citizen"));

#line default
#line hidden
            EndContext();
            BeginContext(5367, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
